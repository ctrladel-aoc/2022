<?php

namespace y2022;

use y2022\src\Day;

require __DIR__ . '/../../autoload.php';

class Day8 extends Day {

  protected const DAY = 8;

  public function __construct() {
    $this->addExample(1, 1, "30373\n25512\n65332\n33549\n35390", 21);
    $this->addExample(2, 1, "30373\n25512\n65332\n33549\n35390", 8);
  }

  public function processInputs(array $inputs): array {

    foreach ($inputs as &$input) {
      $input = str_split($input);
    }

    return $inputs;
  }

  public function getAnswerPart1() {
    $inputs = $this->getInputs();

    $visibleCount = 0;
    foreach ($inputs as $r => $row) {
      $left = 0;
      $right = count($row);
      $top = 0;
      $bottom = count($inputs);

      foreach ($row as $col => $tree) {
        $visible = TRUE;
        for ($i = $left; $i < $col; $i++) {
          if ($row[$i] >= $tree) {
            $visible = FALSE;
            break;
          }
        }

        if ($visible) {
          $visibleCount++;
          continue;
        }

        $visible = TRUE;
        for ($i = $col + 1; $i < $right; $i++) {
          if ($row[$i] >= $tree) {
            $visible = FALSE;
            break;
          }
        }

        if ($visible) {
          $visibleCount++;
          continue;
        }

        $visible = TRUE;
        for ($i = $top; $i < $r; $i++) {
          if ($inputs[$i][$col] >= $tree) {
            $visible = FALSE;
            break;
          }
        }

        if ($visible) {
          $visibleCount++;
          continue;
        }

        $visible = TRUE;
        for ($i = $r + 1; $i < $bottom; $i++) {
          if ($inputs[$i][$col] >= $tree) {
            $visible = FALSE;
            break;
          }
        }

        if ($visible) {
          $visibleCount++;
        }
      }
    }

    $answer = $visibleCount;
    echo "\n\nPART 1\n";
    echo "Answer: $answer";
    return $answer;
  }

  public function getAnswerPart2() {
    $inputs = $this->getInputs();

    $visibility = [];
    foreach ($inputs as $r => $row) {
      $left = 0;
      $right = count($row) - 1;
      $top = 0;
      $bottom = count($inputs) - 1;

      if ($r === $top || $r === $bottom) {
        continue;
      }

      foreach ($row as $col => $tree) {
        if ($col === $left || $col === $right) {
          continue;
        }

        $eyeline = [];

        // Up
        for ($i = $r; $i > $top; $i--) {
          if ($i !== $r && $inputs[$i][$col] >= $tree) {
            break;
          }
        }
        $eyeline[] = $r - $i;

        // Left
        for ($i = $col; $i > $left; $i--) {
          if ($i !== $col && $row[$i] >= $tree) {
            break;
          }
        }
        $eyeline[] = $col - $i;

        // Down
        for ($i = $r; $i < $bottom; $i++) {
          if ($i !== $r && $inputs[$i][$col] >= $tree) {
            break;
          }
        }
        $eyeline[] = $i - $r;

        // Right
        for ($i = $col; $i < $right; $i++) {
          if ($i !== $col && $row[$i] >= $tree) {
            break;
          }
        }
        $eyeline[] = $i - $col;

        $visibility[] = array_product($eyeline);
      }
    }

    sort($visibility);
    $answer = array_pop($visibility);

    echo "\n\nPART 2\n";
    echo "Answer: $answer" ;
    return $answer;
  }

}
