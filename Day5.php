<?php

namespace y2022;

use y2022\src\Day;

require __DIR__ . '/../../autoload.php';

class Day5 extends Day {

  protected const DAY = 5;

  public function __construct() {
    $this->addExample(1, 1, "    [D]    \n[N] [C]    \n[Z] [M] [P]\n 1   2   3 \n\nmove 1 from 2 to 1\nmove 3 from 1 to 3\nmove 2 from 2 to 1\nmove 1 from 1 to 2", "CMZ");
    $this->addExample(2, 1, "    [D]    \n[N] [C]    \n[Z] [M] [P]\n 1   2   3 \n\nmove 1 from 2 to 1\nmove 3 from 1 to 3\nmove 2 from 2 to 1\nmove 1 from 1 to 2", "MCD");
  }

  public function processInputs(array $inputs): array {

    $stack = [];
    $moves = [];
    $row = 1;

    $break = 0;
    foreach ($inputs as $k => $input) {
      if ($input == '') {
        $break = $k;
      }
    }

    for ($i = $break-2; $i >= 0; $i--) {
      $crates = str_split($inputs[$i]);
      $crates = array_chunk($crates, 4);

      foreach ($crates as $k => $crate) {
        if ($crate[1] && $crate[1] !== ' ') {
          $stack[$k+1][$row] = $crate[1];
        }
      }
      $row++;
    }

    for ($i = $break+1; $i < count($inputs); $i++) {
      $moves[] = explode(' ', $inputs[$i]);
    }

    return [
      'stack' => $stack,
      'moves' => $moves,
    ];
  }

  public function getAnswerPart1() {
    $inputs = $this->getInputs();

    $stacks = $inputs['stack'];

    foreach ($inputs['moves'] as $move) {
      $num = $move[1];
      $from = $move[3];
      $to = $move[5];

      $moving = [];
      for ($n = $num; $n > 0; $n--) {
        $moving[] = array_pop($stacks[$from]);
      }

      array_push($stacks[$to], ...$moving);
    }

    $answer = '';
    foreach ($stacks as $stack) {
      $answer .= array_pop($stack);
    }
    echo "\n\nPART 1\n";
    echo "Answer: $answer";
    return $answer;
  }

  public function getAnswerPart2() {
    $inputs = $this->getInputs();

    $stacks = $inputs['stack'];

    foreach ($inputs['moves'] as $move) {
      $num = $move[1];
      $from = $move[3];
      $to = $move[5];

      $moving = array_slice($stacks[$from],$num * -1, $num);
      $stacks[$from] = array_slice($stacks[$from], 0, count($stacks[$from]) - $num);

      array_push($stacks[$to], ...$moving);

    }

    $answer = '';
    foreach ($stacks as $stack) {
      $answer .= array_pop($stack);
    }
    echo "\n\nPART 2\n";
    echo "Answer: $answer" ;
    return $answer;
  }

}
