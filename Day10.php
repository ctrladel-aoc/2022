<?php

namespace y2022;

use aoc\Utility\Grid;
use aoc\Utility\GridCoordinate;
use y2022\src\Day;

require __DIR__ . '/../../autoload.php';

class Day10 extends Day {

  protected const DAY = 10;

  public function __construct() {
    $this->addExample(1, 1, "addx 15\naddx -11\naddx 6\naddx -3\naddx 5\naddx -1\naddx -8\naddx 13\naddx 4\nnoop\naddx -1\naddx 5\naddx -1\naddx 5\naddx -1\naddx 5\naddx -1\naddx 5\naddx -1\naddx -35\naddx 1\naddx 24\naddx -19\naddx 1\naddx 16\naddx -11\nnoop\nnoop\naddx 21\naddx -15\nnoop\nnoop\naddx -3\naddx 9\naddx 1\naddx -3\naddx 8\naddx 1\naddx 5\nnoop\nnoop\nnoop\nnoop\nnoop\naddx -36\nnoop\naddx 1\naddx 7\nnoop\nnoop\nnoop\naddx 2\naddx 6\nnoop\nnoop\nnoop\nnoop\nnoop\naddx 1\nnoop\nnoop\naddx 7\naddx 1\nnoop\naddx -13\naddx 13\naddx 7\nnoop\naddx 1\naddx -33\nnoop\nnoop\nnoop\naddx 2\nnoop\nnoop\nnoop\naddx 8\nnoop\naddx -1\naddx 2\naddx 1\nnoop\naddx 17\naddx -9\naddx 1\naddx 1\naddx -3\naddx 11\nnoop\nnoop\naddx 1\nnoop\naddx 1\nnoop\nnoop\naddx -13\naddx -19\naddx 1\naddx 3\naddx 26\naddx -30\naddx 12\naddx -1\naddx 3\naddx 1\nnoop\nnoop\nnoop\naddx -9\naddx 18\naddx 1\naddx 2\nnoop\nnoop\naddx 9\nnoop\nnoop\nnoop\naddx -1\naddx 2\naddx -37\naddx 1\naddx 3\nnoop\naddx 15\naddx -21\naddx 22\naddx -6\naddx 1\nnoop\naddx 2\naddx 1\nnoop\naddx -10\nnoop\nnoop\naddx 20\naddx 1\naddx 2\naddx 2\naddx -6\naddx -11\nnoop\nnoop\nnoop", 13140);
    $this->addExample(2, 1, "addx 15\naddx -11\naddx 6\naddx -3\naddx 5\naddx -1\naddx -8\naddx 13\naddx 4\nnoop\naddx -1\naddx 5\naddx -1\naddx 5\naddx -1\naddx 5\naddx -1\naddx 5\naddx -1\naddx -35\naddx 1\naddx 24\naddx -19\naddx 1\naddx 16\naddx -11\nnoop\nnoop\naddx 21\naddx -15\nnoop\nnoop\naddx -3\naddx 9\naddx 1\naddx -3\naddx 8\naddx 1\naddx 5\nnoop\nnoop\nnoop\nnoop\nnoop\naddx -36\nnoop\naddx 1\naddx 7\nnoop\nnoop\nnoop\naddx 2\naddx 6\nnoop\nnoop\nnoop\nnoop\nnoop\naddx 1\nnoop\nnoop\naddx 7\naddx 1\nnoop\naddx -13\naddx 13\naddx 7\nnoop\naddx 1\naddx -33\nnoop\nnoop\nnoop\naddx 2\nnoop\nnoop\nnoop\naddx 8\nnoop\naddx -1\naddx 2\naddx 1\nnoop\naddx 17\naddx -9\naddx 1\naddx 1\naddx -3\naddx 11\nnoop\nnoop\naddx 1\nnoop\naddx 1\nnoop\nnoop\naddx -13\naddx -19\naddx 1\naddx 3\naddx 26\naddx -30\naddx 12\naddx -1\naddx 3\naddx 1\nnoop\nnoop\nnoop\naddx -9\naddx 18\naddx 1\naddx 2\nnoop\nnoop\naddx 9\nnoop\nnoop\nnoop\naddx -1\naddx 2\naddx -37\naddx 1\naddx 3\nnoop\naddx 15\naddx -21\naddx 22\naddx -6\naddx 1\nnoop\naddx 2\naddx 1\nnoop\naddx -10\nnoop\nnoop\naddx 20\naddx 1\naddx 2\naddx 2\naddx -6\naddx -11\nnoop\nnoop\nnoop", "##..##..##..##..##..##..##..##..##..##..\n###...###...###...###...###...###...###.\n####....####....####....####....####....\n#####.....#####.....#####.....#####.....\n######......######......######......####\n#######.......#######.......#######.....");
  }

  public function processInputs(array $inputs): array {

    foreach ($inputs as &$input) {
      $input = explode(' ', $input);
    }

    return $inputs;
  }

  public function getAnswerPart1() {
    $inputs = $this->getInputs();

    $x = 1;
    $cycle = 0;
    $nextCheck = 20;
    $checks = [];

    foreach ($inputs as $input) {
      switch ($input[0]) {
        case 'noop':
          $this->cyclePart1($x, $cycle, $checks, $nextCheck);
        break;
        case 'addx':
          $this->cyclePart1($x, $cycle, $checks, $nextCheck);
          $this->cyclePart1($x, $cycle, $checks, $nextCheck);
          $x += $input[1];
        break;
      }
    }

    $answer = array_sum($checks);
    echo "\n\nPART 1\n";
    echo "Answer: $answer";
    return $answer;
  }

  public function getAnswerPart2() {
    $inputs = $this->getInputs();

    $crt = new Grid();
    $pos = 0;
    for ($y = 0; $y < 6; $y++) {
      for ($x = 0; $x < 40; $x++) {
        $p = new GridCoordinate($x, $y, 0, ['pos' => $pos, 'p' => '.']);
        $crt->addPoint($p);
        $pos++;
      }
    }

    $sprite = 1;
    $spritePos = -1;
    $crtPos = 0;
    $cycle = 0;

    foreach ($inputs as $input) {
      switch ($input[0]) {
        case 'noop':
          $this->cyclePart2($sprite, $cycle, $crt, $crtPos, $spritePos);
          break;
        case 'addx':
          $this->cyclePart2($sprite, $cycle, $crt, $crtPos, $spritePos);
          $this->cyclePart2($sprite, $cycle, $crt, $crtPos, $spritePos);

          $sprite += $input[1];
          break;
      }
    }

    $answer = $crt->printMeta([0, 39], [0, 5], [0,0], '.','p')[0];
    echo "\n\nPART 2\n";
    echo "Answer: \n$answer" ;
    return $answer;
  }

  protected function cyclePart1(int &$x, int &$cycle, array &$checks, int &$nextCheck) {
    $cycle++;
    if ($cycle === $nextCheck) {
      $checks[] = $x * $cycle;
      $nextCheck += 40;
    }
  }

  protected function cyclePart2(int &$sprite, int &$cycle, Grid &$crt, int &$crtPos, int &$spritePos) {
    $cycle++;
    /** @var \aoc\Utility\GridCoordinate $pixel */
    $pixel = $crt->getPointByMeta('pos', $crtPos)[0] ?? FALSE;
    if ($pixel) {
      $pixelPos = $pixel->getMetaProperty('pos');
      if (($pixelPos - 1)%40 === $sprite || $pixelPos%40 === $sprite || ($pixelPos)%40 + 1 === $sprite) {
        $pixel->setMetaProperty('p', '#');
        $spritePos = ($spritePos + 1);
        $spritePos = $spritePos > 1 ? -1 : $spritePos;
      }
    }
    $crtPos++;
  }
}
