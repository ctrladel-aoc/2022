<?php

namespace y2022;

use aoc\Utility\Grid;
use aoc\Utility\GridCoordinate;
use y2022\src\Day;

require __DIR__ . '/../../autoload.php';

class Day9 extends Day {

  protected const DAY = 9;

  public function __construct() {
    $this->addExample(1, 1, "R 4
U 4
L 3
D 1
R 4
D 1
L 5
R 2", 13);
    $this->addExample(2, 1, "R 5
U 8
L 8
D 3
R 17
D 10
L 25
U 20", 36);
  }

  public function processInputs(array $inputs): array {

    foreach ($inputs as &$input) {
      $input = explode(' ', $input);
    }

    return $inputs;
  }

  public function getAnswerPart1() {
    $inputs = $this->getInputs();

    $visited = new Grid();
    $start = new GridCoordinate(0,0, 0);
    $visited->addPoint($start);
    $startVisits = 0;

    $head = new GridCoordinate(0,0, 0, ['p' => 'H', 'head' => TRUE, 'tail' => TRUE]);

    $moves = new Grid();
    $moves->addPoint($head);

    $moves->printMeta([],[],[],'.','p');

    foreach ($inputs as $input) {
      $direction = $input[0];
      $steps = $input[1];

      for ($i = 0; $i < $steps; $i++) {
        /** @var GridCoordinate $ohead */
        $ohead = clone $moves->getPointByMeta('head', TRUE)[0];
        $otail = clone $moves->getPointByMeta('tail', TRUE)[0];

        $head = clone $ohead;
        $tail = clone $otail;

        $head->setMeta(['p' => 'H', 'head' => TRUE, 'tail' => FALSE]);
        $ohead->setMetaProperty('head', FALSE);

        $tail->setMeta(['p' => 'T', 'head' => FALSE, 'tail' => TRUE]);
        $otail->setMetaProperty('tail', FALSE);

        switch ($direction) {
          case 'R':
            $head->addX(1);
          break;
          case 'L':
            $head->addX(-1);
            break;
          case 'D':
            $head->addY(-1);
            break;
          case 'U':
            $head->addY(1);
            break;
        }

        if ($moves->getAdjacentPoints($head)) {
          $nextToTail = FALSE;
          foreach ($moves->getAdjacentPoints($head) as $point) {
            if ($point->getMetaProperty('tail')) {
              $nextToTail = TRUE;
            }
          }

          if ($head->getCoordinateKey() === $tail->getCoordinateKey()) {
            $nextToTail = TRUE;
          }

          if (!$nextToTail) {
            if ($head->getX() !== $tail->getX() && $head->getY() !== $tail->getY()) {
              $ha = $moves->getAdjacentCoords($head);
              $ta = $moves->getAdjacentCoords($tail);

              $possible = array_intersect_key($ha, $ta);

              foreach ($possible as $point) {
                if ($point->getX() !== $tail->getX() && $point->getY() !== $tail->getY()) {
                  $tail->setX($point->getX());
                  $tail->setY($point->getY());
                  break;
                }
              }
              $a = 5;
            }
            elseif ($head->getX() === $tail->getX()) {
              if ($head->getY() > $tail->getY()) {
                $move = 1;
              }
              else {
                $move = -1;
              }
              $tail->addY($move);
            }
            elseif ($head->getY() === $tail->getY()) {
              if ($head->getX() > $tail->getX()) {
                $move = 1;
              }
              else {
                $move = -1;
              }
              $tail->addX($move);
            }
          }
        }

        if ($head->getCoordinateKey() === $tail->getCoordinateKey()) {
          $head->setMetaProperty('tail', TRUE);
          $moves->addPoint($head);
          $visited->addPoint($head);
        }
        else {
          $moves->addPoint($head);
          $moves->addPoint($tail);
          $visited->addPoint($tail);
        }

        if ($ohead->getCoordinateKey() !== $head->getCoordinateKey() && $ohead->getCoordinateKey() !== $tail->getCoordinateKey()) {
          $moves->removePoint($ohead);
        }

        if (($otail->getCoordinateKey() !== $head->getCoordinateKey() && $otail->getCoordinateKey() !== $tail->getCoordinateKey())) {
          $moves->removePoint($otail);
        }
      }
    }

    $answer = count($visited->getPoints());
    echo "\n\nPART 1\n";
    echo "Answer: $answer";
    return $answer;
  }

  public function getAnswerPart2() {
    $inputs = $this->getInputs();

    $size = 26;
    $grid = new Grid();
    for ($x = -$size; $x < $size; $x++) {
      for ($y = -$size; $y < $size; $y++) {
        $p = new GridCoordinate($x, $y, 0, [
          'p' => '.',
          0 => FALSE,
          1 => FALSE,
          2 => FALSE,
          3 => FALSE,
          4 => FALSE,
          5 => FALSE,
          6 => FALSE,
          7 => FALSE,
          8 => FALSE,
          9 => FALSE,
          'visited' => 0,
        ]);
        $grid->addPoint($p);
      }
    }

    $visited = new Grid();

    $start = new GridCoordinate(0,0, 0,
      [
        'p' => 'H',
        0 => TRUE,
        1 => TRUE,
        2 => TRUE,
        3 => TRUE,
        4 => TRUE,
        5 => TRUE,
        6 => TRUE,
        7 => TRUE,
        8 => TRUE,
        9 => TRUE,
      ]
    );

    $grid->addPoint($start);

    foreach ($inputs as $input) {
      $direction = $input[0];
      $steps = $input[1];

      for ($i = 0; $i < $steps; $i++) {

        /** @var GridCoordinate $head */
        $head = $grid->getPointByMeta(0, TRUE)[0];

        switch ($direction) {
          case 'R':
            $nHead = $grid->getPoint($head->getX() + 1, $head->getY(), $head->getZ());
            break;
          case 'L':
            $nHead = $grid->getPoint($head->getX() - 1, $head->getY(), $head->getZ());
            break;
          case 'D':
            $nHead = $grid->getPoint($head->getX(), $head->getY() - 1, $head->getZ());
            break;
          case 'U':
            $nHead = $grid->getPoint($head->getX(), $head->getY() + 1, $head->getZ());
            break;
        }

        $head->setMetaProperty(0, FALSE);
        $this->updateMeta($head);
        $nHead->setMetaProperty(0, TRUE);
        $this->updateMeta($nHead);

        for ($l = 0; $l < 9; $l++ ) {
          $this->moveSeg($grid, $l, "{$direction}{$steps}.{$i}.{$l}");
        }

        $visited->addPoint($grid->getPointByMeta(9, TRUE)[0]);
      }
    }

    $answer = count($visited->getPoints());
    echo "\n\nPART 2\n";
    echo "Answer: $answer" ;
    return $answer;
  }

  protected function moveSeg(Grid $grid, $headPos, $move) {
    $tailPos = $headPos + 1;
    /** @var GridCoordinate $head */
    $head = $grid->getPointByMeta($headPos, TRUE)[0];
    /** @var GridCoordinate $tail */
    $tail = $grid->getPointByMeta($tailPos, TRUE)[0];

    if ($head->getCoordinateKey() !== $tail->getCoordinateKey() && !isset($grid->getAdjacentCoords($head)[$tail->getCoordinateKey()])) {
      $point = NULL;
      if ($head->getX() !== $tail->getX() && $head->getY() !== $tail->getY()) {
        $ha = $grid->getAdjacentCoords($head);
        $ta = $grid->getAdjacentCoords($tail);

        $possible = array_intersect_key($ha, $ta);

        foreach ($possible as &$p) {
          if ($p->getX() !== $tail->getX() && $p->getY() !== $tail->getY()) {
            $point = $p;
            break;
          }
        }
        $point = $grid->getPoint($point->getX(), $point->getY(), $point->getZ());
      }
      elseif ($head->getX() === $tail->getX()) {
        if ($head->getY() > $tail->getY()) {
          $move = 1;
        }
        else {
          $move = -1;
        }

        $point = $grid->getPoint($tail->getX(), $tail->getY() + $move, $tail->getZ());
      }
      elseif ($head->getY() === $tail->getY()) {
        if ($head->getX() > $tail->getX()) {
          $move = 1;
        }
        else {
          $move = -1;
        }
        $point = $grid->getPoint($tail->getX() + $move, $tail->getY(), $tail->getZ());
      }

      $tail->setMetaProperty($tailPos, FALSE);
      $this->updateMeta($tail);
      $point->setMetaProperty($tailPos, TRUE);
      $this->updateMeta($point);
    }
  }

  public function updateMeta(GridCoordinate $point) {
    $meta = $point->getMeta();

    $meta['p'] = '.';
    for ($i = 0; $i <= 9; $i++) {
      if (isset($meta[$i]) && $meta[$i] === TRUE) {
        $meta['p'] = $i === 0 ? 'H' : $i;
        break;
      }
    }

    $point->setMeta($meta);
  }

}