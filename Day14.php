<?php

namespace y2022;

use aoc\Utility\Grid;
use aoc\Utility\GridCoordinate;
use y2022\src\Day;

require __DIR__ . '/../../autoload.php';

class Day14 extends Day {

  protected const DAY = 14;

  public function __construct() {
    $this->addExample(1, 1, "498,4 -> 498,6 -> 496,6
503,4 -> 502,4 -> 502,9 -> 494,9", 24);
    $this->addExample(2, 1, "498,4 -> 498,6 -> 496,6
503,4 -> 502,4 -> 502,9 -> 494,9", 93);
  }

  public function processInputs(array $inputs): array {

    $points = [];
    foreach ($inputs as &$input) {
      $paths = explode(' -> ', $input);

      for ($i = 0; $i < count($paths) - 1; $i++) {
        [$sX, $sY] = explode(',', $paths[$i]);
        [$fX, $fY] = explode(',', $paths[$i+1]);

        if ($sX !== $fX) {
          $ssX = $sX > $fX ? $fX : $sX;
          $ffX = $sX > $fX ? $sX : $fX;
          for ($x = $ssX; $x <= $ffX; $x++) {
            $p = new GridCoordinate($x, $sY, 0, ['mark' => '#']);
            $points[$p->getCoordinateKey()] = $p;
          }
        }

        if ($sY !== $fY) {
          $ssY = $sY > $fY ? $fY : $sY;
          $ffY = $sY > $fY ? $sY : $fY;
          for ($y = $ssY; $y <= $ffY; $y++) {
            $p = new GridCoordinate($sX, $y, 0, ['mark' => '#']);
            $points[$p->getCoordinateKey()] = $p;
          }
        }
        $a = 5;
      }
    }

    return $points;
  }

  public function getAnswerPart1() {
    $inputs = $this->getInputs();

    $grid = new Grid();
    foreach ($inputs as $input) {
      $grid->addPoint($input);
    }
    $p = new GridCoordinate(500, 0, 0, ['mark' => '+']);
    $grid->addPoint($p);

    $blockers = $this->buildBlockers($grid);

//    $c = 1;
    do {
      $stopped = $this->dropSand($grid, $blockers);
//      if ($c % 100 === 0) {
//        echo "\n" . $grid->printMeta([], [], [], '.', 'mark')[0] . "\n";
//      }
//      $c++;
    } while ($stopped === FALSE && $grid->getPoint(500,0,0)->getMetaProperty('mark') === '+');

    $answer = count($grid->getPointByMeta('mark', 'o'));
    echo "\n\nPART 1\n";
    echo "Answer: $answer";
    return $answer;
  }

  public function getAnswerPart2() {
    $inputs = $this->getInputs();

    $grid = new Grid();
    foreach ($inputs as $input) {
      $grid->addPoint($input);
    }
    $p = new GridCoordinate(500, 0, 0, ['mark' => '+']);
    $grid->addPoint($p);

    $ys = $grid->getYs();
    $line = max($ys) + 2;

    $blockers = $this->buildBlockers($grid);

//    $c = 1;
    do {
      $stopped = $this->dropSand($grid, $blockers, $line);
//      if ($c % 1000 === 0) {
//        echo "\n" . $grid->printMeta([], [], [], '.', 'mark')[0] . "\n";
//      }
//      $c++;
    } while ($stopped === FALSE && $grid->getPoint(500,0,0)->getMetaProperty('mark') === '+');

    $answer = count($grid->getPointByMeta('mark', 'o'));
    echo "\n\nPART 2\n";
    echo "Answer: $answer" ;
    return $answer;
  }

  protected function buildBlockers($grid) {
    $blockers = [];
    foreach ($grid->getPointByMeta('mark', '#') as $blocker) {
      $blockers[$blocker->getX()][$blocker->getY()] = [
        'x' => $blocker->getX(),
        'y' => $blocker->getY(),
      ];
    }

    return $blockers;
  }

  protected function dropSand(Grid $grid, array &$blockers, $part2 = FALSE) {
    $dropAt = new GridCoordinate(500,0,0);

    $moving = TRUE;
    while ($moving) {
      $stopAt = $this->getBlocker($blockers, $dropAt, $part2);
      if ($stopAt === NULL) {
        return TRUE;
      }

      $p = new GridCoordinate($stopAt['x'], $stopAt['y'] - 1, 0);

      $adj = [];
      if (isset($blockers[$stopAt['x'] - 1][$stopAt['y']])) {
        $adj['nw'] = TRUE;
      }
      if (isset($blockers[$stopAt['x'] + 1][$stopAt['y']])) {
        $adj['ne'] = TRUE;
      }

      if ($stopAt['y'] === $part2) {
        $adj['nw'] = TRUE;
        $adj['ne'] = TRUE;
      }
      if (!isset($adj['nw'])) {
        $dropAt = new GridCoordinate($p->getX() - 1, $p->getY() + 1, 0, ['mark' => 'o']);
        $moving = TRUE;
      }
      elseif (!isset($adj['ne'])) {
        $dropAt = new GridCoordinate($p->getX() + 1, $p->getY() + 1, 0, ['mark' => 'o']);
        $moving = TRUE;
      }
      else {
        $p->setMetaProperty('mark', 'o');
        $dropAt = $p;
        $moving = FALSE;
        $grid->addPoint($dropAt);
        $blockers[$dropAt->getX()][$dropAt->getY()] = [
          'x' => $dropAt->getX(),
          'y' => $dropAt->getY(),
        ];
      }
    }

    return FALSE;
  }

  protected function getBlocker(&$blockers, $point, $part2 = FALSE) {
    $stopAt = NULL;

    $px = $point->getX();
    $py = $point->getY();
    if (isset($blockers[$px][$py + 1])) {
      return $blockers[$px][$py + 1];
    }
    elseif (isset($blockers[$px])) {
      $maxY = max(array_keys($blockers[$px]));

      for ($y = $py + 1; $y <= $maxY; $y++ ) {
        if (!isset($blockers[$px][$y])) {
          continue;
        }
        $stopAt = $blockers[$px][$y];
        break;
      }
    }

    if ($stopAt === NULL && $part2) {
      $stopAt = ['x' => $px, 'y' => $part2];
    }

    return $stopAt;
  }

}
