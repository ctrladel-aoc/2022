<?php

namespace y2022;

use aoc\Utility\Grid;
use aoc\Utility\GridCoordinate;
use y2022\src\Day;

require __DIR__ . '/../../autoload.php';

class Day12 extends Day {

  protected const DAY = 12;

  private array $chars;

  public function __construct() {
    $this->addExample(1, 1, "Sabqponm\nabcryxxl\naccszExk\nacctuvwj\nabdefghi", 31);
    $this->addExample(2, 1, "Sabqponm\nabcryxxl\naccszExk\nacctuvwj\nabdefghi", 29);
    $this->chars = array_flip(range('a', 'z'));
    $this->chars['E'] = 25;
    $this->chars['S'] = 0;
  }

  public function processInputs(array $inputs): array {

    return $inputs;
  }

  public function getAnswerPart1() {
    $inputs = $this->getInputs();
    $grid = new Grid();

    foreach ($inputs as $y => $row) {
      $chars = str_split($row);

      foreach ($chars as $x => $col) {
        $p = new GridCoordinate($x, $y, 0, ['mark' => $col]);

          $grid->addPoint($p);
      }
    }

//    echo "\n" . $grid->printMeta([],[],[],' ', 'mark')[0] . "\n";

    $start = $grid->getPointByMeta('mark', 'S')[0];
    $dest = $grid->getPointByMeta('mark', 'E')[0];

    $queue = [
      [
        'point' => $start,
        'weight' => 0
      ],
    ];

    while (count($queue)) {
      $this->findPathNext($dest, $grid, $queue, $visited);
    }

    $answer = $dest->getMetaProperty('steps');
    echo "\n\nPART 1\n";
    echo "Answer: $answer";
    return $answer;
  }

  public function getAnswerPart2() {
    $inputs = $this->getInputs();
    $grid = new Grid();

    foreach ($inputs as $y => $row) {
      $chars = str_split($row);
      foreach ($chars as $x => $col) {
        $p = new GridCoordinate($x, $y, 0, ['mark' => $col]);

        $grid->addPoint($p);
      }
    }

//    echo "\n" . $grid->printMeta([],[],[],' ', 'mark')[0] . "\n";

    $starts = $grid->getPointByMeta('mark', 'a');
    $starts[] = $grid->getPointByMeta('mark', 'S')[0];

    $dest = $grid->getPointByMeta('mark', 'E')[0];
    usort($starts, function ($a, $b) use ($grid, $dest) {
      return $grid->getManhattanDistance($b, $dest) <=> $grid->getManhattanDistance($a, $dest);
    });

    $currentMin = 1000000;
    foreach ($starts as $start) {
      $grid = new Grid();

      foreach ($inputs as $y => $row) {
        $chars = str_split($row);
        foreach ($chars as $x => $col) {
          $p = new GridCoordinate($x, $y, 0, ['mark' => $col]);

          $grid->addPoint($p);
        }
      }

      $visited = [];
      $pathDest = $grid->getPointByMeta('mark', 'E')[0];

      $queue = [
        [
          'point' => $start,
          'weight' => 0
        ],
      ];

      while (count($queue)) {
        $this->findPathNext($pathDest, $grid, $queue, $visited);

        if ($queue && $queue[array_key_first($queue)]['point']->getMetaProperty('steps') > $currentMin) {
          break;
        }
      }

      if ($pathDest->getMetaProperty('steps')) {
        $currentMin = $pathDest->getMetaProperty('steps') < $currentMin ? $pathDest->getMetaProperty('steps') : $currentMin;
      }
    }

    $answer = $currentMin;
    echo "\n\nPART 2\n";
    echo "Answer: $answer" ;
    return $answer;
  }

  protected function findPathNext(GridCoordinate $dest, Grid &$grid, &$queue, &$visited = []) {
    $start = array_shift($queue);
    $start = $start['point'];

    if ($start->getCoordinateKey() === $dest->getCoordinateKey()) {
      return;
    }

    $possible = $grid->getAdjacentPoints($start, FALSE);
    $possible = $this->check($start, $possible, $visited);

    if (!$possible) {
      return;
    }

    foreach ($possible as &$next) {
      $currentCost = $start->getMetaProperty('steps');
      $newCost = $start->getMetaProperty('steps') + 1;
      $nextCost = $next->getMetaProperty('steps');

      if (is_null($nextCost) || $newCost < $currentCost) {
        $next->setMetaProperty('steps', $newCost);
        $weight = $grid->getManhattanDistance($next, $dest) + $newCost;
        $queue[] = [
          'point' => $next,
          'weight' => $weight,
        ];
        $visited[$next->getCoordinateKey()] = $next->getMetaProperty('mark');
      }
    }

    usort($queue, function ($a, $b) use ($grid, $dest) {
      return $a['weight'] <=> $b['weight'];
    });
  }

  /**
   * @param $current
   * @param \aoc\Utility\GridCoordinate[] $possible
   *
   * @return array
   */
  protected function check(GridCoordinate $current, array $possible, $visited) {
    $results = [];
    $cV = $current->getMetaProperty('mark');
    if ($cV === 'S') {
      $cV = 'a';
    }
    $cV = $this->chars[$cV];
    $cP = $cV + 1;

    foreach ($possible as &$char) {
      if ($current->getCoordinateKey() === $char->getCoordinateKey()) {
        continue;
      }

      $pV = $char->getMetaProperty('mark');
      if ($pV === 'S') {
        continue;
      }

      $pV = $this->chars[$pV];

      if ($pV <= $cP) {
        $results[] = $char;
      }
    }

    return $results;
  }

}
