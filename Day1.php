<?php

namespace y2022;

use y2022\src\Day;

require __DIR__ . '/../../autoload.php';

class Day1 extends Day {

  protected const DAY = 1;

  protected int $target;

  public function __construct() {
    $this->addExample(1, 1, "1000\n2000\n3000\n\n4000\n\n5000\n6000\n\n7000\n8000\n9000\n\n10000", 24000);
    $this->addExample(2, 1, "1000\n2000\n3000\n\n4000\n\n5000\n6000\n\n7000\n8000\n9000\n\n10000", 45000);
  }

  public function getAnswerPart1() {
    $inputs = $this->getInputs();

    $elves = $this->getCalorieCount($inputs);

    $answer = max($elves);
    echo "\nAnswer: $answer";

    return $answer;
  }

  public function getAnswerPart2() {
    $inputs = $this->getInputs();

    $elves = $this->getCalorieCount($inputs);

    sort($elves);
    $threeMost = array_slice($elves, -3,3);
    $answer = array_sum($threeMost);

    echo "\nAnswer: $answer";

    return $answer;
  }

  public function getCalorieCount($input): array {
    $elves = [];
    $elf = 1;
    $calories = 0;
    foreach ($input as $index => $item) {
      if (!$item) {
        $elves[$elf] = $calories;
        $calories = 0;
        $elf++;
      }
      elseif ($index === count($input) - 1) {
        $elves[$elf] = (int) $item;
      }
      else {
        $calories += $item;
      }
    }

    return $elves;
  }

}
