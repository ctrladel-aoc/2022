<?php

namespace y2022;

use aoc\Utility\Grid;
use aoc\Utility\GridCoordinate;
use y2022\src\Day;

require __DIR__ . '/../../autoload.php';

class Day18 extends Day {

  protected const DAY = 18;

  public function __construct() {
    $this->addExample(1, 1, "1,1,1
2,1,1", "10");
    $this->addExample(1, 2, "2,2,2
1,2,2
3,2,2
2,1,2
2,3,2
2,2,1
2,2,3
2,2,4
2,2,6
1,2,5
3,2,5
2,1,5
2,3,5", "64");
    $this->addExample(2, 1, "2,2,2
1,2,2
3,2,2
2,1,2
2,3,2
2,2,1
2,2,3
2,2,4
2,2,6
1,2,5
3,2,5
2,1,5
2,3,5", "58");
  }

  public function processInputs(array $inputs): array {

    foreach ($inputs as &$input) {
      $input = explode(',', $input);
    }

    return $inputs;
  }

  public function getAnswerPart1() {
    $inputs = $this->getInputs();

    $grid = new Grid();

    foreach ($inputs as $input) {
      $p = new GridCoordinate($input[0],$input[1],$input[2]);
      $grid->addPoint($p);
    }

    $exposed = 0;
    foreach ($grid->getPoints() as $point) {
      $adj = $grid->getAdjacentPoints($point, FALSE, TRUE);

      $exposed += 6 - count($adj);
    }

    $answer = $exposed;
    echo "\n\nPART 1\n";
    echo "Answer: $answer";
    return $answer;
  }

  public function getAnswerPart2() {
    $inputs = $this->getInputs();

    $grid = new Grid();

    // Create a grid with the known rock blocks.
    foreach ($inputs as $input) {
      $p = new GridCoordinate($input[0],$input[1],$input[2], ['block' => TRUE, 'mark' => '#']);
      $grid->addPoint($p);
    }

    $bb = $grid->findBoundingBox();

    // Fill in the rest of the grid with 'empty' blocks.
    $points = $grid->getPoints();
    for ($x = $bb['min']->getX(); $x <= $bb['max']->getX(); $x++) {
      for ($y = $bb['min']->getY(); $y <= $bb['max']->getY(); $y++) {
        for ($z = $bb['min']->getY(); $z <= $bb['max']->getZ(); $z++) {
          $p = new GridCoordinate($x,$y,$z, ['empty' => TRUE, 'exposed' => '?', 'mark' => '.']);
          if (!isset($points[$p->getCoordinateKey()])) {
            $grid->addPoint($p);
          }
        }
      }
    }

    // Build groups of empty blocks.
    $groupCount = 1;
    $groups = [];
    $points = [];
    do {
      $changed = FALSE;

      // @TODO this is slow.
      foreach ($grid->getPointByMeta('empty', TRUE) as $point) {
        $adj = $grid->getAdjacentPoints($point, FALSE, TRUE);

        // Get the existing groups of all the adjacent blocks.
        $startingGroup = $points[$point->getCoordinateKey()] ?? NULL;
        $touching = [$point];
        $seenGroups = [];
        foreach ($adj as $item) {
          if ($item->getMetaProperty('empty')) {
            $touching[] = $item;
            if (isset($points[$item->getCoordinateKey()])) {
              $seenGroups[] = $points[$item->getCoordinateKey()];
            }
          }
        }

        // Create a new group, keep the current group, merge groups that are now
        // touching.
        $group = NULL;
        $seenGroups = array_unique($seenGroups);
        if ($startingGroup === NULL && count($seenGroups) === 0) {
          $group = $groupCount;
          $groupCount++;
        }
        elseif ($startingGroup !== NULL && count($seenGroups) === 0) {
          $group = $startingGroup;
        }
        elseif(count($seenGroups) === 1) {
          $group = reset($seenGroups);
        }
        elseif(count($seenGroups) > 1) {
          $biggestGroup = 0;
          foreach($seenGroups as $g) {
            $biggestGroup = $biggestGroup < count($groups[$g]) ? $g : $biggestGroup;
          }

          $group = $biggestGroup;
          foreach ($seenGroups as $k => $g) {
            if ($g === $biggestGroup) {
              continue;
            }

            $groups[$biggestGroup] = array_merge($groups[$g], $groups[$biggestGroup]);

            foreach ($groups[$biggestGroup] as $q => $w) {
              $groups[$biggestGroup][$q] = $biggestGroup;
              $points[$q] = $biggestGroup;
            }

            unset($groups[$g]);
          }
        }

        // Update all the blocks with the new group
        if ($group !== $startingGroup || count($seenGroups) > 1) {
          foreach ($touching as $t) {
            $groups[$group][$t->getCoordinateKey()] = $group;
          }

          foreach($groups[$group] as $k => $p) {
            $points[$k] = $group;
          }

          $changed = TRUE;
        }
      }
    }while($changed);

    // Set all the exposed block on all the empty blocks.
    foreach ($grid->getPointByMeta('exposed', '?') as $point) {
      $exposed = FALSE;
      if ($point->getX() === $bb['min']->getX() || $point->getX() === $bb['max']->getX()) {
        $exposed = TRUE;
      }
      if ($point->getY() === $bb['min']->getY() || $point->getY() === $bb['max']->getY()) {
        $exposed = TRUE;
      }
      if ($point->getZ() === $bb['min']->getZ() || $point->getZ() === $bb['max']->getZ()) {
        $exposed = TRUE;
      }

      // If this block is exposed then every block in its group is to.
      if ($exposed) {
        $group = $points[$point->getCoordinateKey()];
        foreach($groups[$group] as $k => $e) {
          $yp = strpos($k, 'y');
          $zp = strpos($k, 'z');

          $x = (int) substr($k, 1, $yp - 1);
          $y = (int) substr($k, $yp + 1, $zp - $yp);
          $z = (int) substr($k, $zp + 1);

          // Remove the block from the grid we don't care about it.
          $ep = $grid->getPoint($x, $y, $z);
          if ($ep) {
            $grid->removePoint($ep);
          }
        }
      }
    }

//    for ($z = $bb['min']->getZ(); $z < $bb['max']->getZ(); $z++) {
//      echo "\nZ:$z\n" . $grid->printMeta([], [], [], '_', 'mark')[$z] . "\n";
//    }

    // Internal voids are now filled with a placeholder block. Count exposed sides.
    $exposed = 0;
    foreach ($grid->getPointByMeta('block', TRUE) as $kyle) {
      $adj = $grid->getAdjacentPoints($kyle, FALSE, TRUE);

      $exposed += 6 - count($adj);
    }

    $answer = $exposed;
    echo "\n\nPART 2\n";
    echo "Answer: $answer" ;
    echo "\n" ;
    return $answer;
  }

}
