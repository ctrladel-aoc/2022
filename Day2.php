<?php

namespace y2022;

use y2022\src\Day;

require __DIR__ . '/../../autoload.php';

class Day2 extends Day {

  protected const DAY = 2;

  public function __construct() {
    $this->addExample(1, 1, "A Y\nB X\nC Z", '15');
    $this->addExample(2, 1, "A Y\nB X\nC Z", '12');
  }

  public function getAnswerPart1() {
    $inputs = $this->getInputs();

    $scores = [
      'AX' => 4,
      'AY' => 8,
      'AZ' => 3,
      'BX' => 1,
      'BY' => 5,
      'BZ' => 9,
      'CX' => 7,
      'CY' => 2,
      'CZ' => 6,
    ];

    $score = 0;
    foreach ($inputs as $input) {
      $play = str_replace(' ', '', $input);

      if (isset($scores[$play])) {
        $score += $scores[$play];
      }

    }

    $answer = $score;
    echo "\nAnswer: $answer";
    return $answer;
  }

  public function getAnswerPart2() {
    $inputs = $this->getInputs();

    $scores = [
      'AX' => 3,
      'AY' => 4,
      'AZ' => 8,
      'BX' => 1,
      'BY' => 5,
      'BZ' => 9,
      'CX' => 2,
      'CY' => 6,
      'CZ' => 7,
    ];

    $score = 0;
    foreach ($inputs as $input) {
      $play = str_replace(' ', '', $input);

      if (isset($scores[$play])) {
        $score += $scores[$play];
      }

    }

    $answer = $score;
    echo "\nAnswer: $answer";
    return $answer;
  }

}
