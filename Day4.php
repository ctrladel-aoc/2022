<?php

namespace y2022;

use y2022\src\Day;

require __DIR__ . '/../../autoload.php';

class Day4 extends Day {

  protected const DAY = 4;

  public function __construct() {
    $this->addExample(1, 1, "2-4,6-8\n2-3,4-5\n5-7,7-9\n2-8,3-7\n6-6,4-6\n2-6,4-8", 2);
    $this->addExample(2, 1, "2-4,6-8\n2-3,4-5\n5-7,7-9\n2-8,3-7\n6-6,4-6\n2-6,4-8", 4);
  }

  public function processInputs(array $inputs): array {
    foreach ($inputs as &$input) {
      $input = explode(',', $input);
    }
    return $inputs;
  }

  public function getAnswerPart1() {
    $inputs = $this->getInputs();

    $overlap = 0;
    foreach ($inputs as $input) {
      [$min1,$max1] = explode('-', $input[0]);
      [$min2,$max2] = explode('-', $input[1]);

      if ($min1 >= $min2 && $max1 <= $max2) {
        $overlap++;
      }
      elseif ($min2 >= $min1 && $max2 <= $max1) {
        $overlap++;
      }

    }

    $answer = $overlap;
    echo "\nAnswer: $answer";
    return $answer;
  }

  public function getAnswerPart2() {
    $inputs = $this->getInputs();

    $overlap = 0;
    foreach ($inputs as $input) {
      [$min1,$max1] = explode('-', $input[0]);
      $seq1 = range($min1, $max1);

      [$min2,$max2] = explode('-', $input[1]);
      $seq2 = range($min2, $max2);

      $same = array_intersect($seq1, $seq2);

      if ($same) {
        $overlap++;
      }

    }

    $answer = $overlap;
    echo "\n\nPART 2\n";
    echo "Answer: $answer" ;
    return $answer;
  }

}
