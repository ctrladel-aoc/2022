<?php

namespace tests2022;

use y2022\Day13 as Day;
use y2022\src\DayInterface;

final class Day13Test extends DayTestBase {

  protected function setUp(): void {
    $this->day = new Day();
  }

  protected static function getDay(): DayInterface {
    return new Day();
  }

}