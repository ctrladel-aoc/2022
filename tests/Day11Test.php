<?php

namespace tests2022;

use y2022\Day11 as Day;
use y2022\src\DayInterface;

final class Day11Test extends DayTestBase {

  protected function setUp(): void {
    $this->day = new Day();
  }

  protected static function getDay(): DayInterface {
    return new Day();
  }

}