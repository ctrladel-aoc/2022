<?php

namespace tests2022;

use y2022\Day1 as Day;
use y2022\src\DayInterface;

final class Day1Test extends DayTestBase {

  protected DayInterface $day;

  protected function setUp(): void {
    $this->day = new Day();
  }

  protected static function getDay(): DayInterface {
    return new Day();
  }

}