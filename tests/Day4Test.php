<?php

namespace tests2022;

use y2022\Day4 as Day;
use y2022\src\DayInterface;

final class Day4Test extends DayTestBase {

  protected function setUp(): void {
    $this->day = new Day();
  }

  protected static function getDay(): DayInterface {
    return new Day();
  }

}