<?php

namespace tests2022;

use PHPUnit\Framework\TestCase;
use y2022\Day22 as Day;
use y2022\src\DayInterface;
use y2022\src\ExampleInterface;

final class Day22Test extends TestCase {

  protected DayInterface $day;

  protected function setUp(): void {
    $this->day = new Day();
  }

}