<?php

namespace tests2022;

use PHPUnit\Framework\TestCase;
use y2022\src\DayInterface;
use y2022\src\ExampleInterface;

abstract class DayTestBase extends TestCase {

  protected DayInterface $day;

  protected abstract static function getDay(): DayInterface;

  /**
   * @dataProvider example1Provider
   */
  public function testExample1(ExampleInterface $example) {
    echo "\nExample " . $example->getNumber() . ':';
    $this->day->setInputs($example->getInput());
    $answer = $this->day->getAnswerPart1(...$example->getArgs());

    $this->assertEquals($example->getAnswer(), $answer);
  }

  /**
   * @dataProvider part1Provider
   */
  public function testPart1($expected) {
    echo "\nPart 1:";
    $answer = $this->day->getAnswerPart1();
    $this->assertEquals($expected, $answer);
  }

  /**
   * @dataProvider example2Provider
   */
  public function testExample2(ExampleInterface $example) {
    echo "\nExample " . $example->getNumber() . ':';
    $this->day->setInputs($example->getInput());
    $answer = $this->day->getAnswerPart2(...$example->getArgs());

    $this->assertEquals($example->getAnswer(), $answer);
  }

  /**
   * @dataProvider part2Provider
   */
  public function testPart2($expected) {
    echo "\nPart 2:";
    $answer = $this->day->getAnswerPart2();
    $this->assertEquals($expected, $answer);
  }

  public static function example1Provider(): array {
    $day = static::getDay();
    $examples = $day->getExamples();
    foreach ($examples[1] as &$example) {
      $example = [$example];
    }

    return $examples[1];
  }

  public static function example2Provider(): array {
    $day = static::getDay();
    $examples = $day->getExamples();
    foreach ($examples[2] as &$example) {
      $example = [$example];
    }

    return $examples[2];
  }

  public static function part1Provider(): array {
    $day = static::getDay();
    $answers = $day->getAnswers();

    $answer[] = [$answers[1]];
    return $answer;
  }

  public static function part2Provider(): array {
    $day = static::getDay();
    $answers = $day->getAnswers();

    $answer[] = [$answers[2]];
    return $answer;
  }

}