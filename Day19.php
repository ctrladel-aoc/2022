<?php

namespace y2022;

use y2022\src\Day;

require __DIR__ . '/../../autoload.php';

class Day19 extends Day {

  protected const DAY = 19;

  public function __construct() {
    $this->addExample(1, 1, "", "");
    $this->addExample(2, 1, "", "");
  }

  public function processInputs(array $inputs): array {

    return $inputs;
  }

  public function getAnswerPart1() {
    $inputs = $this->getInputs();

    $answer = '';
    echo "\n\nPART 1\n";
    echo "Answer: $answer";
    return $answer;
  }

  public function getAnswerPart2() {
    $inputs = $this->getInputs();

    $answer = '';
    echo "\n\nPART 2\n";
    echo "Answer: $answer" ;
    return $answer;
  }

}

$day = new DayN();
//$day->setInputs($day->getExample('1.1')->getInput());
$day->getAnswerPart1();
$day->getAnswerPart2();
