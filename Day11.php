<?php

namespace y2022;

use y2022\src\Day;

require __DIR__ . '/../../autoload.php';

class Day11 extends Day {

  protected const DAY = 11;

  public function __construct() {
    $this->addExample(1, 1, "Monkey 0:\n  Starting items: 79, 98\n  Operation: new = old * 19\n  Test: divisible by 23\n    If true: throw to monkey 2\n    If false: throw to monkey 3\n\nMonkey 1:\n  Starting items: 54, 65, 75, 74\n  Operation: new = old + 6\n  Test: divisible by 19\n    If true: throw to monkey 2\n    If false: throw to monkey 0\n\nMonkey 2:\n  Starting items: 79, 60, 97\n  Operation: new = old * old\n  Test: divisible by 13\n    If true: throw to monkey 1\n    If false: throw to monkey 3\n\nMonkey 3:\n  Starting items: 74\n  Operation: new = old + 3\n  Test: divisible by 17\n    If true: throw to monkey 0\n    If false: throw to monkey 1", 10605);
    $this->addExample(2, 1, "Monkey 0:\n  Starting items: 79, 98\n  Operation: new = old * 19\n  Test: divisible by 23\n    If true: throw to monkey 2\n    If false: throw to monkey 3\n\nMonkey 1:\n  Starting items: 54, 65, 75, 74\n  Operation: new = old + 6\n  Test: divisible by 19\n    If true: throw to monkey 2\n    If false: throw to monkey 0\n\nMonkey 2:\n  Starting items: 79, 60, 97\n  Operation: new = old * old\n  Test: divisible by 13\n    If true: throw to monkey 1\n    If false: throw to monkey 3\n\nMonkey 3:\n  Starting items: 74\n  Operation: new = old + 3\n  Test: divisible by 17\n    If true: throw to monkey 0\n    If false: throw to monkey 1", 2713310158);
  }

  public function processInputs(array $inputs): array {
    $monkeys = [];
    $monkey = 0;

    foreach ($inputs as $input) {
      $input = trim($input);

      if (str_starts_with($input, 'Monkey')) {
        [$m, $monkey] = explode(' ', $input);
        $monkey = (int) str_replace(':', '', $monkey);
      }
      elseif (str_starts_with($input, 'Starting')) {
        [$m, $items] = explode(': ', $input);
        $items = explode(',', $items);
        $monkeys[$monkey]['items'] = $items;
      }
      elseif (str_starts_with($input, 'Operation')) {
        [$m, $op] = explode(': ', $input);
        $operation = str_replace('new = ', '', $op);

        str_replace('throw to monkey ', '', $operation);

        $op = '';
        if (str_contains($operation, '*')) {
          $op = '*';
        }
        if (str_contains($operation, '+')) {
          $op = '+';
        }

        [$one, $two] = explode(" $op ", $operation);

        $monkeys[$monkey]['op'] = [
          'op' => $op,
          'one' => $one,
          'two' => $two
        ];
      }
      elseif (str_starts_with($input, 'Test')) {
        [$m, $test] = explode(': ', $input);
        $monkeys[$monkey]['test'] = (int) str_replace('divisible by ', '', $test);
      }
      elseif (str_starts_with($input, 'If true')) {
        [$m, $test] = explode(': ', $input);
        $monkeys[$monkey]['true'] = (int) str_replace('throw to monkey ', '', $test);
      }
      elseif (str_starts_with($input, 'If false')) {
        [$m, $test] = explode(': ', $input);
        $monkeys[$monkey]['false'] = (int) str_replace('throw to monkey ', '', $test);
      }
    }

    return $monkeys;
  }

  public function getAnswerPart1() {
    $inputs = $this->getInputs();

    for ($i = 0; $i < 20; $i++) {
      $this->playRound($inputs, TRUE);
    }

    $inspections = [];
    foreach ($inputs as $k => $input) {
      $inspections[$k] = $input['inspections'];
    }

    sort($inspections);

    $top = array_pop($inspections);
    $second = array_pop($inspections);

    $answer = $top * $second;
    echo "\n\nPART 1\n";
    echo "Answer: $answer";
    return $answer;
  }

  public function getAnswerPart2() {
    $inputs = $this->getInputs();

    $mod = 1;
    foreach ($inputs as $monkey) {
      $mod *= $monkey['test'];
    }

    for ($i = 0; $i < 10000; $i++) {
      $this->playRound($inputs, FALSE, $mod);
    }

    $inspections = [];
    foreach ($inputs as $k => $input) {
      $inspections[$k] = $input['inspections'];
    }

    sort($inspections);

    $top = array_pop($inspections);
    $second = array_pop($inspections);

    $answer = $top * $second;

    echo "\n\nPART 2\n";
    echo "Answer: $answer" ;
    return $answer;
  }

  protected function playRound(&$monkeys, $relief, $mod = 1) {
    foreach ($monkeys as $m => &$monkey) {
      foreach ($monkey['items'] as $i => &$item) {
        if (!isset($monkey['inspections'])) {
          $monkey['inspections'] = 0;
        }
        $monkey['inspections']++;

        $item = $this->monkeyInspect($item, $monkey['op']);
        if ($relief) {
          $item = $this->relief($item);
        }
        else {
          $item %= $mod;
        }

        $nextMonkey = $this->monkeyTest($item, $monkey['test'], $monkey['true'], $monkey['false']);
        $monkeys[$nextMonkey]['items'][] = $item;
      }
      $monkeys[$m]['items'] = [];
    }
  }

  protected function monkeyInspect($item, $op) {
    $one = $op['one'] === 'old' ? $item : (int) $op['one'];
    $two = $op['two'] === 'old' ? $item : (int) $op['two'];

    switch ($op['op']) {
      case '+':
        $item = $one + $two;

        break;
      case '*':
        $item = $one * $two;
        break;
    }

    return $item;
  }

  protected function relief($item) {
    return floor($item/3);
  }

  protected function monkeyTest($item, $test, $true, $false) {
    if ($item % $test === 0) {
      return $true;
    }
    else {
      return $false;
    }
  }

}
