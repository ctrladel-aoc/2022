<?php

namespace y2022;

use y2022\src\Day;

require __DIR__ . '/../../autoload.php';

class Day6 extends Day {

  protected const DAY = 6;

  public function __construct() {
    $this->addExample(1, 1, "mjqjpqmgbljsphdztnvjfqwrcgsmlb", 7);
    $this->addExample(1, 2, "bvwbjplbgvbhsrlpgdmjqwftvncz", 5);
    $this->addExample(1, 3, "nppdvjthqldpwncqszvftbrmjlhg", 6);
    $this->addExample(1, 4, "nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg", 10);
    $this->addExample(1, 5, "zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw", 11);
    $this->addExample(2, 1, "mjqjpqmgbljsphdztnvjfqwrcgsmlb", 19);
    $this->addExample(2, 2, "bvwbjplbgvbhsrlpgdmjqwftvncz", 23);
    $this->addExample(2, 3, "nppdvjthqldpwncqszvftbrmjlhg", 23);
    $this->addExample(2, 4, "nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg", 29);
    $this->addExample(2, 5, "zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw", 26);
  }

  public function processInputs(array $inputs): array {

    return $inputs;
  }

  public function getAnswerPart1() {
    $inputs = $this->getInputs();

    $inputs = str_split($inputs[0]);
    $start = $this->findUniqueSequence($inputs, 4);

    $answer = count($start);
    echo "\n\nPART 1\n";
    echo "Answer: $answer";
    return $answer;
  }

  public function getAnswerPart2() {
    $inputs = $this->getInputs();

    $inputs = str_split($inputs[0]);
    $start = $this->findUniqueSequence($inputs, 14);

    $answer = count($start);
    echo "\n\nPART 2\n";
    echo "Answer: $answer" ;
    return $answer;
  }

  protected function findUniqueSequence(array $inputs, int $numUnique): array {
    $processed = [];

    foreach ($inputs as $input) {
      $processed[] = $input;
      $unique = array_slice($processed, -$numUnique, $numUnique);

      if (count(array_unique($unique)) === $numUnique) {
        break;
      }
    }

    return $processed;
  }

}
