<?php

namespace y2022;

use y2022\src\Day;

require __DIR__ . '/../../autoload.php';

class Day3 extends Day {

  protected const DAY = 3;

  public function __construct() {
    $this->addExample(1, 1, "vJrwpWtwJgWrhcsFMMfFFhFp\njqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL\nPmmdzqPrVvPwwTWBwg\nwMqvLMZHhHMvwLHjbvcjnnSBnvTQFn\nttgJtRGJQctTZtZT\nCrZsJsPPZsGzwwsLwLmpwMDw", 157);
    $this->addExample(2, 1, "vJrwpWtwJgWrhcsFMMfFFhFp\njqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL\nPmmdzqPrVvPwwTWBwg\nwMqvLMZHhHMvwLHjbvcjnnSBnvTQFn\nttgJtRGJQctTZtZT\nCrZsJsPPZsGzwwsLwLmpwMDw", 70);
  }

  public function getAnswerPart1() {
    $inputs = $this->getInputs();

    $priorityItems = [];
    foreach ($inputs as $input) {
      $middle = strlen($input)/2 ;

      $comp1 = str_split(substr($input, 0, $middle));
      $comp2 = str_split(substr($input, $middle));
      $same = array_intersect($comp1, $comp2);
      $same = reset($same);

      $priorityItems[] = [
        'letter' => $same,
        'value' => $this->getCharValue($same),
      ];
    }

    $answer = 0;
    foreach ($priorityItems as $item) {
      $answer += $item['value'];
    }
    echo "\n\nPART 1\n";
    echo "Answer: $answer";
    return $answer;
  }

  public function getAnswerPart2() {
    $inputs = $this->getInputs();

    $priorityItems = [];
    for($i = 0; $i < count($inputs); $i +=3) {
      $first = str_split($inputs[$i]);
      $second = str_split($inputs[$i + 1]);
      $third = str_split($inputs[$i + 2]);

      $same = array_intersect($first, $second, $third);
      $same = reset($same);

      $priorityItems[]= [
        'letter' => $same,
        'value' => $this->getCharValue($same),
      ];
    }

    $answer = 0;
    foreach ($priorityItems as $item) {
      $answer += $item['value'];
    }
    echo "\n\nPART 2\n";
    echo "Answer: $answer" ;
    return $answer;
  }

  protected function getCharValue(string $char): int {
    $upperArr = range('A', 'Z') ;
    $LowerArr = range('a', 'z') ;

    if (ctype_upper($char)) {
      $v = array_search($char, $upperArr) + 27;
    }
    else {
      $v = array_search($char, $LowerArr) + 1;
    }

    return $v;
  }

}
