<?php

namespace y2022;

use aoc\Utility\NestedArray;
use y2022\src\Day;

require __DIR__ . '/../../autoload.php';

class Day7 extends Day {

  protected const DAY = 7;

  public function __construct() {
    $this->addExample(1, 1, "$ cd /\n$ ls\ndir a\n14848514 b.txt\n8504156 c.dat\ndir d\n$ cd a\n$ ls\ndir e\n29116 f\n2557 g\n62596 h.lst\n$ cd e\n$ ls\n584 i\n$ cd ..\n$ cd ..\n$ cd d\n$ ls\n4060174 j\n8033020 d.log\n5626152 d.ext\n7214296 k", 95437);
    $this->addExample(2, 1, "$ cd /\n$ ls\ndir a\n14848514 b.txt\n8504156 c.dat\ndir d\n$ cd a\n$ ls\ndir e\n29116 f\n2557 g\n62596 h.lst\n$ cd e\n$ ls\n584 i\n$ cd ..\n$ cd ..\n$ cd d\n$ ls\n4060174 j\n8033020 d.log\n5626152 d.ext\n7214296 k", 24933642);
  }

  public function processInputs(array $inputs): array {
    $map = ['/' => []];
    $currentPath = [];

    foreach ($inputs as $input) {
      if ($input[0] === '$') {
        $val = explode(' ', $input);
        $command = $val[1];
        $dir = $val[2] ?? '';

        switch ($command) {
          case 'cd':
            if ($dir ===  '/') {
              $currentPath = ['/'];
            }
            else if ($dir === '..') {
              array_pop($currentPath);
            }
            else {
              $currentPath[] = $dir;
            }
            break;
          default:
            continue 2;
        }
      }
      else {
        [$one, $two] = explode(' ', $input);
        $current = NestedArray::getValue($map, $currentPath);

        $current[$two] = $one;
        if ($one === 'dir') {
          $current[$two] = [];
        }

        NestedArray::setValue($map, $currentPath, $current);
      }
    }

    return $map;
  }

  public function getAnswerPart1() {
    $inputs = $this->getInputs();

    $sizes = [];
    $this->getDirSizes($inputs, $sizes);
    $sizes = $this->sumDirs($sizes);

    $size = 0;
    foreach($sizes as $dir) {
      if ($dir <= 100000) {
        $size += $dir;
      }
    }

    $answer = $size;
    echo "\n\nPART 1\n";
    echo "Answer: $answer";
    return $answer;
  }

  public function getAnswerPart2() {
    $inputs = $this->getInputs();

    $sizes = [];
    $this->getDirSizes($inputs, $sizes);
    $sizes = $this->sumDirs($sizes);

    $needed = 30000000 - (70000000 - $sizes['/']);
    $currentChoice = 10000000000;
    foreach($sizes as $dir) {
      if ($dir >= $needed && $dir < $currentChoice ) {
        $currentChoice = $dir;
      }
    }

    $answer = $currentChoice;
    echo "\n\nPART 2\n";
    echo "Answer: $answer" ;
    return $answer;
  }

  protected function getDirSizes($directory, &$map = [], $currentPath = ''): int {
    $size = 0;
    foreach ($directory as $k => $contents) {
      if (is_array($contents)) {
        $path = $currentPath ? $currentPath . '|' . $k : $k;
        $currentPath = $currentPath ?: $k;

        if (!isset($map[$path])) {
          $map[$path] = 0;
        }

        $size = $this->getDirSizes($contents, $map, $path);
        $map[$currentPath] += $size;
      }
      else {
        $map[$currentPath] += $contents;
      }
    }

    return $size;
  }

  public function sumDirs($directories): array {
    ksort($directories);
    $original = $directories;
    foreach ($directories as $path => &$size) {
      foreach ($original as $path2 => $size2) {
        if ($path !== $path2 && str_starts_with($path2, $path)) {
          $size += $size2;
        }
      }
    }

    return $directories;
  }

}
