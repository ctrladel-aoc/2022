<?php


namespace y2022\src;


interface ExampleInterface {

  public function setInput(string $input);

  public function getInput(): string;

  public function getAnswer(): string;

  public function getNumber(): string;

  /**
   * @return array
   */
  public function getArgs(): array;

}