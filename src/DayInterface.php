<?php


namespace y2022\src;


interface DayInterface {

  /**
   * @param string $separator
   *
   * @return array
   */
  public function getInputs(string $separator = "\n"): array;

  public function getAnswers(): array;

  public function setInputs(string $inputs);

  public function processInputs(array $inputs): array;

  public function getAnswerPart1();

  public function getAnswerPart2();

  public function addExample(int $part, int $number, string $input, string $answer, array $args): void;

  public function getExamples(): array;

}